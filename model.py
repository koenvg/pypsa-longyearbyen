# SPDX-FileCopyrightText: : 2021 Koen van Greevenbroek and Lars Klein
# SPDX-License-Identifier: GPL-3.0-or-later

"""
PyPSA-Longyearbyen.

A simple energy system model for Longyearbyen, built with PyPSA.
"""

from typing import List

import pandas as pd
import pypsa
import yaml

from utilities import load_parameters


def add_components(
    parameters: pd.DataFrame, config: dict, data: pd.DataFrame, turbines: List[str]
):
    """Create a PyPSA network following the given configuration.

    Parameters
    ----------
    parameters: pd.DataFrame
        Cost and efficiency parameters for the components in the model.
    config: dict
        Configuration for the model, including modelling horizon.
    data: pd.DataFrame
        A pandas DataFrame indexed over the modelling horizon
        (n.snapshots) with columns "AC load", "heat load", "wind capacity"
        and "PV capacity" giving generation and consumption timeseries.
    turbines: List[str]
        A list of which wind turbines to include in the model. Each
        element in the list must be a column in the `data` DataFrame.
    """

    n = pypsa.Network()
    snapshots = pd.date_range(
        start=config["snapshots"]["start"],
        end=config["snapshots"]["end"],
        freq="1h",
    )
    n.set_snapshots(snapshots)

    n.add("Carrier", ["AC", "heat", "hydrogen"])

    # Busses
    n.add("Bus", "AC bus", carrier="AC")
    if config["system"]["heat_enabled"]:
        n.add("Bus", "heat bus", carrier="heat")

    # Load
    n.add("Load", "electricity demand", bus="AC bus", p_set=data["AC load"])
    if config["system"]["heat_enabled"]:
        n.add("Load", "heat demand", bus="heat bus", p_set=data["heat load"])

    # Generators
    for t in turbines:
        n.add(
            "Generator",
            t,
            bus="AC bus",
            p_nom_max=parameters.at["onwind", "p_nom_max"],
            p_nom_extendable=True,
            p_max_pu=data[t],
            capital_cost=parameters.at["onwind", "capital_cost"],
        )

    n.add(
        "Generator",
        "PV",
        bus="AC bus",
        p_nom_max=parameters.at["solar", "p_nom_max"],
        p_nom_extendable=True,
        p_max_pu=data["PV capacity"],
        capital_cost=parameters.at["solar", "capital_cost"],
    )

    # Load curtailment / penalty generation in case of otherwise infeasible model formulations
    n.add(
        "Generator",
        "load curtailment AC",
        bus="AC bus",
        p_nom=10000,
        marginal_cost=parameters.at["curtailment", "marginal_cost"],
    )

    if config["system"]["heat_enabled"]:
        n.add(
            "Generator",
            "load curtailment heat",
            bus="heat bus",
            p_nom=10000,
            marginal_cost=parameters.at["curtailment", "marginal_cost"],
        )

    # Batteries
    n.add(
        "StorageUnit",
        "batteries",
        bus="AC bus",
        p_nom_extendable=True,
        max_hours=config["storage"]["max_hours"]["battery"],
        capital_cost=parameters.at["battery", "capital_cost"],
    )

    # H2
    n.add("Bus", "hydrogen bus", carrier="hydrogen")
    n.add(
        "Store",
        "hydrogen storage",
        bus="hydrogen bus",
        e_cyclic=True,
        standing_loss=parameters.at["hydrogen storage", "standing loss"],
        e_nom_extendable=True,
        capital_cost=parameters.at["hydrogen storage", "capital_cost"],
    )
    n.add(
        "Link",
        "electrolysis",
        bus0="AC bus",
        bus1="hydrogen bus",
        efficiency=parameters.at["electrolysis", "efficiency"],
        p_nom_extendable=True,
        capital_cost=parameters.at["electrolysis", "capital_cost"],
    )
    n.add(
        "Link",
        "fuel cell",
        bus0="hydrogen bus",
        bus1="AC bus",
        efficiency=parameters.at["fuel cell", "efficiency"],
        p_nom_extendable=True,
        capital_cost=parameters.at["fuel cell", "capital_cost"],
    )

    if config["system"]["heat_enabled"]:
        # Heat pump directly to district heating:
        n.add(
            "Link",
            "heat pump to heat",
            bus0="AC bus",
            bus1="heat bus",
            p_nom_extendable=True,
            efficiency=parameters.at["central heat pump", "efficiency"],
            capital_cost=parameters.at["central heat pump", "capital_cost"],
        )

        if config["storage"]["thermal_enabled"]:
            # Hot water thermal storage
            n.add("Bus", "hot water storage bus", carrier="heat")
            n.add(
                "Store",
                "hot water storage",
                bus="hot water storage bus",
                e_cyclic=True,
                standing_loss=parameters.at["hot water storage", "standing loss"],
                e_nom_extendable=True,
                capital_cost=parameters.at["hot water storage", "capital_cost"],
            )
            n.add(
                "Link",
                "heat pump to hot water storage",
                bus0="AC bus",
                bus1="hot water storage bus",
                p_nom_extendable=True,
                efficiency=parameters.at["central heat pump", "efficiency"],
                capital_cost=parameters.at["central heat pump", "capital_cost"],
            )
            n.add(
                "Link",
                "hot water heat exchanger out",
                bus0="hot water storage bus",
                bus1="heat bus",
                efficiency=parameters.at["heat exchanger", "efficiency"],
                p_nom_extendable=True,
                capital_cost=parameters.at["heat exchanger", "capital_cost"],
            )

            # Hot rocks thermal storage
            n.add("Bus", "hot rocks storage bus", carrier="heat")
            n.add(
                "Store",
                "hot rocks storage",
                bus="hot rocks storage bus",
                e_cyclic=True,
                standing_loss=parameters.at["hot rocks storage", "standing loss"],
                e_nom_extendable=True,
                capital_cost=parameters.at["hot rocks storage", "capital_cost"],
            )
            n.add(
                "Link",
                "hot rocks charger",
                bus0="AC bus",
                bus1="hot rocks storage bus",
                efficiency=parameters.at["resistive heater", "efficiency"],
                p_nom_extendable=True,
                capital_cost=parameters.at["resistive heater", "capital_cost"],
            )
            n.add(
                "Link",
                "hot rocks heat exchanger out",
                bus0="hot rocks storage bus",
                bus1="heat bus",
                p_nom_extendable=True,
                efficiency=parameters.at["heat exchanger", "efficiency"],
                capital_cost=parameters.at["heat exchanger", "capital_cost"],
            )
            n.add(
                "Link",
                "hot rocks thermal generator",
                bus0="hot rocks storage bus",
                bus1="AC bus",
                p_nom_extendable=True,
                efficiency=parameters.at["hot storage thermal generator", "efficiency"],
                capital_cost=parameters.at[
                    "hot storage thermal generator", "capital_cost"
                ],
            )

            # Molten salt thermal storage
            n.add("Bus", "molten salt storage bus", carrier="heat")
            n.add(
                "Store",
                "molten salt storage",
                bus="molten salt storage bus",
                e_cyclic=True,
                standing_loss=parameters.at["molten salt storage", "standing loss"],
                e_nom_extendable=True,
                capital_cost=parameters.at["molten salt storage", "capital_cost"],
            )
            n.add(
                "Link",
                "molten salt charger",
                bus0="AC bus",
                bus1="molten salt storage bus",
                p_nom_extendable=True,
                efficiency=parameters.at["resistive heater", "efficiency"],
                capital_cost=parameters.at["resistive heater", "capital_cost"],
            )
            n.add(
                "Link",
                "molten salt heat exchanger out",
                bus0="molten salt storage bus",
                bus1="heat bus",
                efficiency=parameters.at["heat exchanger", "efficiency"],
                p_nom_extendable=True,
                capital_cost=parameters.at["heat exchanger", "capital_cost"],
            )
            n.add(
                "Link",
                "molten salt thermal generator",
                bus0="molten salt storage bus",
                bus1="AC bus",
                p_nom_extendable=True,
                efficiency=parameters.at["hot storage thermal generator", "efficiency"],
                capital_cost=parameters.at[
                    "hot storage thermal generator", "capital_cost"
                ],
            )

    # Resample the time series data to the desired resolution
    offset = config["snapshots"]["resolution"]
    if offset != "1h":
        snapshot_weightings = n.snapshot_weightings.resample(offset).sum()
        n.set_snapshots(snapshot_weightings.index)
        for c in n.iterate_components():
            pnl = getattr(n, c.list_name + "_t")
            for k, df in c.pnl.items():
                if not df.empty:
                    pnl[k] = df.resample(offset).mean()

    return n


def create_model(config: dict = None, solve: bool = False):
    """Create the PyPSA-Longyearbyen model.

    This is the top-level function used to create the
    PyPSA-Longyearbyen model. It loads configuration and data, then
    calls the `add_components` function to create a pypsa.Network
    instance. Optionally solves the resulting model.

    Parameters
    ----------
    config: dict
        An alternative configuration for the model, in the same format
        as the `config.yml` file.

    """
    # Load the model data, parameters and configuration.
    if not config:
        with open("config.yml", "r") as stream:
            config = yaml.safe_load(stream)

    parameters = load_parameters(
        "parameters.csv", config["costs"], config["storage"], Nyears=1.0
    )

    # Load AC and heat load factors.
    data = pd.read_csv("data/LYR_load_full.csv", index_col=0, parse_dates=True)

    # Load hourly PV capacity factors.
    data_PV = pd.read_csv(
        "data/LYR_solar_supply.csv", index_col="time", parse_dates=[0, 1]
    )
    data.loc[:, "PV capacity"] = data_PV["electricity"]

    # Load hourly wind capacity factors.
    data_wind = pd.read_csv(
        "data/LYR_wind_supply.csv", index_col="time", parse_dates=True
    )
    data = data.join(data_wind)
    data.fillna(0, inplace=True)
    turbines = [
        loc + " " + t for loc in ["AD", "BR", "LH"] for t in config["wind"]["turbines"]
    ]

    # Create the network
    n = add_components(parameters, config, data, turbines)

    if solve:
        n.optimize(
            solver_name=config["solving"]["solver"],
            solver_options=config["solving"]["options"],
        )

    return n
