# Data documentation

## Load time series
Hourly AC and heat loads for Longyearbyen are given in `LYR_load_full.csv`.
The first column contains the timestamps of the measurements in `YYYY-MM-DD HH:MM:SS` format.
The second and third columns contains the AC and heat loads respectively, in MW.

The `original_load/LYR_AC_load_original.csv` and `original_load/LYR_heat_load_original.csv` contain the same data as originally received, without timestamps and in kW.
The original data is for the period `2017-01-01 00:00:00` to `2018-12-31 23:00:00` in hourly resolution.
This data was repeated in two-year intervals to generate a load profile for the entire 2011-2020 period.

## Capacity factor time series
### Wind
Hourly wind capacity factors are given for several locations in `LYR_wind_supply.csv`, which covers a period from 2011 to 2020, inclusive.
The first column contains timestamps in the `YYYY-MM-DD HH:MM:SS` format.
Each remaining column contains the wind capacity factors for a particular location and wind turbine over the given timestamps.
Each column name follows the scheme `<location> <turbine>`.
Here, `<location>` is one of `AD`, `BR` or `LH`, standing for Adventdalen, Breinosa and Lufthavn, respectively.
Meanwhile, `<turbine>` is one of `V25`, `V47`, `TUGE50`, `TUGE10` or `MPR`, representing different wind turbine models.

The capacity factors were calculated on the basis of actual wind measurements, but there are some gaps in the wind measurement data.
Therefore, make sure to fill undefined values with zeroes when for example loading the data as a pandas.DataFrame by calling `fillna(0, inplace=True)` on the DataFrame.


### Solar
Hourly PV capacity factors are given in the `LYR_solar_supply.csv` file.
The first two columns contain time steps in the `YYYY-MM-DD HH:MM:SS` format; one for UTC time and one for local time.
The third column lists the corresponding capacity factor between 0 and 1 (unitless) for each time step.


## Data licences

Load data was obtained from the Longyearbyen coal power plant with written permission to publish and share the data, but without an actual license.

Wind capacity factors were obtained by Aleksander Grochowicz, Daniel Heineken and Sondre Wennberg, and the data is under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) (CC BY 4.0) licence.

PV capacity factors were obtained by Maximilian Roithner and Håvar Alexandersen using [renewables.ninja](https://renewables.ninja), and the data is under the [Creative Commons Attribution-NonCommercial 4.0 International](https://creativecommons.org/licenses/by-nc/4.0/) (CC BY-NC 4.0) licence.
