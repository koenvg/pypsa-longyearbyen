# SPDX-FileCopyrightText: : 2021 Koen van Greevenbroek and Lars Klein
# SPDX-License-Identifier: GPL-3.0-or-later

"""Utility functions supporting the PyPSA-Longyearbyen model building."""


import math

import pandas as pd


def load_parameters(parameters, cost_config, storage_config, Nyears=1.0):
    """Process a pandas DataFrame with cost and efficiency parameters.

    Adapted from PyPSA-Eur (https://github.com/PyPSA/pypsa-eur/blob/7b68e8be0caa131cbbf3fdea19ff158da9e74400/scripts/add_electricity.py)

    Parameters
    ----------
    parameters: String
        The name of a .csv file containing costs and other parameters.
    cost_config: Dict
        Cost configuration (discount, lifetime, etc.)
    storage_config: Dict
        Storage configuration: capacity / effect ratio (max_hours) for
        batteries and hydrogen storage. Used the `Storage` components.
    Nyears: Float
        The number of years to model.
    """
    # Set all asset costs and other parameters
    costs = pd.read_csv(parameters, index_col=list(range(3))).sort_index()

    # Correct units to MW and EUR.
    costs.loc[costs.unit.str.contains("/kW"), "value"] *= 1e3
    costs.loc[costs.unit.str.contains("USD"), "value"] *= cost_config["USD_to_EUR"]

    # Convert standing losses in %/day to hourly factors
    costs.loc[costs.unit.str.contains("%/day"), "value"] = costs.loc[
        costs.unit.str.contains("%/day"), "value"
    ].apply(lambda x: math.pow(1 + x / 100.0, 1.0 / 24) - 1)

    # Index the dataframe solely by technology.
    costs = (
        costs.loc[pd.IndexSlice[:, cost_config["year"], :], "value"]
        .unstack(level=2)
        .groupby("technology")
        .sum(min_count=1)
    )

    # Create missing columns.
    costs["CO2 intensity"] = None
    costs["discount rate"] = None
    costs["fuel"] = None

    # Fill in missing default values.
    costs = costs.fillna(
        {
            "CO2 intensity": 0,
            "FOM": 0,
            "VOM": 0,
            "discount rate": cost_config["discountrate"],
            "efficiency": 1,
            "fuel": 0,
            "investment": 0,
            "lifetime": 25,
        }
    )

    # Calculate annuatised capital costs based on lifetime and
    # discount rate.
    costs["capital_cost"] = (
        (annuity(costs["lifetime"], costs["discount rate"]) + costs["FOM"] / 100.0)
        * costs["investment"]
        * Nyears
    )

    # Not needed unless we add gas turbines to the model:
    # costs.at['OCGT', 'fuel'] = costs.at['gas', 'fuel']
    # costs.at['CCGT', 'fuel'] = costs.at['gas', 'fuel']

    # Calculate marginal costs.
    costs["marginal_cost"] = costs["VOM"] + costs["fuel"] / costs["efficiency"]

    # Clean up CO2 naming.
    costs = costs.rename(columns={"CO2 intensity": "co2_emissions"})

    # Not needed unless we add gas turbines to the model:
    # costs.at['OCGT', 'co2_emissions'] = costs.at['gas', 'co2_emissions']
    # costs.at['CCGT', 'co2_emissions'] = costs.at['gas', 'co2_emissions']

    # Calculate battery and H2 storage costs in the case of
    # `StorageUnit`s with fixed capacity/storage ratios (given by
    # `max_hours`).
    def costs_for_storage(store, link1, link2=None, max_hours=1.0):
        capital_cost = link1["capital_cost"] + max_hours * store["capital_cost"]
        if link2 is not None:
            capital_cost += link2["capital_cost"]
        return pd.Series(
            dict(capital_cost=capital_cost, marginal_cost=0.0, co2_emissions=0.0)
        )

    max_hours = storage_config["max_hours"]
    costs.loc["battery"] = costs_for_storage(
        costs.loc["battery storage"],
        costs.loc["battery inverter"],
        max_hours=max_hours["battery"],
    )
    costs.loc["H2"] = costs_for_storage(
        costs.loc["hydrogen storage"],
        costs.loc["fuel cell"],
        costs.loc["electrolysis"],
        max_hours=max_hours["H2"],
    )

    return costs


def annuity(n, r=0):
    """Calculate an annuity factor.

    Calculate the annuity factor for an asset with lifetime n years and
    discount rate of r, e.g. annuity(20,0.05)*20 = 1.6

    Taken from vresutils at https://github.com/FRESNA/vresutils/blob/c475d28deb49c6124e1445e3c611b6aa9048d255/vresutils/costdata.py
    """
    if isinstance(r, pd.Series):
        return pd.Series(1 / n, index=r.index).where(
            r == 0, r / (1.0 - 1.0 / (1.0 + r) ** n)
        )
    elif r > 0:
        return r / (1.0 - 1.0 / (1.0 + r) ** n)
    else:
        return 1 / n
