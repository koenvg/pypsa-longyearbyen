# PyPSA-Longyearbyen

A simple open model for Longyearbyen including the electricity and heating sectors, implemented using PyPSA.
A report based on this model can be found at https://hdl.handle.net/10037/22287.

## Setup

Create a conda environment for the PyPSA-Longyearbyen by running
```shell
conda env create -f envs/environment.yml
```

By default the model is configured to use the Gurobi solver.
(See https://www.gurobi.com/academia/academic-program-and-licenses/ for academic licences.)

However, it is easily configurable to use an open source linear program solver such as GLPK or CBC by changing the `solver_name` argument for the `pypsa.Network.lopf` function.
To install these two solvers, simply run `conda install -c conda-forge coincbc` or `conda install -c conda-forge glpk` in the `pypsa-longyearbyen` conda environment.

## Running the model

Activate the conda environment (`conda activate pypsa-longyearbyen`) and open a python shell (`python`) or Jupyter notebook.
Load the module (`import model`) and build and solve the model by calling `n = model.create_model()`.

Running the model over a 1 year period using the Gurobi solver takes about half a minute.
Running the model over the full 9 year period using the Gurobi solver can take 5-10 minutes and 2-3 GB of memory.

## Analysing the model

Once a model has been created and solved using `create_model()`, the resulting `pypsa.Network` object can be inspected and analysed.
Some example analyses are given in the `Analysis*.ipynb` notebooks.

Some solved models are provided in the `models` directory.
These include a 9 year run of the model (`LYR_2011_2019.nc`) and a few variations run with a different configuration of with different parameters.
The variations are:
- `LYR_2011_2016_no_thermal_storage.nc`: no thermal storage.
- `LYR_2011_2019_bad_heat_pump.nc`: the coefficient of performance of the heat pump is reduced from 3.0 to 2.0.
- `LYR_2011_2019_exp_thermal_storage.nc`: the price of thermal storage units is doubled.
- `LYR_2011_2019_exp_wind.nc`: the price of wind turbines is increased by 50%.

To load a solved model, simple `import pypsa` and run e.g. `n = pypsa.Network("models/LYR_2011_2019.nc")`.

## Licenses

All code in this repository is licenced under the [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html) or later.
The licencing of the data used to run the model is documented in `data/README.md`.
All other supplementary data in this repository (solved models, figures, etc.) are licenced under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) (CC BY 4.0) licence.

## Background

This model was created by Koen van Greevenbroek and Lars Klein for the course "AGF-853 Sustainable Arctic Energy Exploration and Development" at UNIS, Svalbard.

